package de.wumpitz.darc.auth.service;

import de.wumpitz.darc.auth.model.User;

public interface UserService {
    void save(User user);
    User findByUsername(String username);
}
