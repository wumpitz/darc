package de.wumpitz.darc.auth.repository;

import de.wumpitz.darc.auth.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
