package de.wumpitz.darc.auth.repository;

import de.wumpitz.darc.auth.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
